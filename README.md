# bluemon

Tool for detecting changes to folder of LabVIEW code and automatically running the blue formatter for every save action.

## Table Of Contents

[TOC]

## Installation

```  bash
# first install blue itself - there is no VIPM package yet so just clone the repository
git clone https://gitlab.com/blue
# clone this repo
git clone https://gitlab.com/sas-blue/bluemon
# create a new venv - note requires Python 3.11 or later
python3.11 -m venv venv
# then activate it - depends or your shell
# for Git Bash
source venv/Scripts/activate
# for PowerShell
.\venv\Scripts\activate.ps1
# for cmd
venv\Scripts\activate.bat
# Then install bluemon 
python3 -m pip install .
# Now run bluemon
bluemon
# You should see a GUI pop up
```

## Usage

On each subsequent run, you'll need to activate the venv, then run `bluemon`. You could easily write a shell script to do that for you and place it on the Desktop for easy access.