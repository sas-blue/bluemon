import PySimpleGUI as sg  # type: ignore
import watchdog.events
import watchdog.observers
from pathlib import Path
from .gcli import format_file

FORMAT_VI_LOCATION = Path("blue")


class Handler(watchdog.events.PatternMatchingEventHandler):
    def __init__(self):
        # Set the patterns for PatternMatchingEventHandler
        watchdog.events.PatternMatchingEventHandler.__init__(
            self, patterns=["*.vi"], ignore_directories=True, case_sensitive=False
        )

    def on_created(self, event):
        format_file(event.src_path)

    def on_modified(self, event):
        format_file(event.src_path)


def main():
    event_handler = Handler()
    observers = {}
    global stdoutput
    folders = []
    layout = [
        [
            sg.Listbox([], enable_events=True, size=(110, 10), key="-FOLDER LIST-"),
            sg.Column(
                [
                    [
                        sg.In(visible=False, enable_events="True", key="-ADDFOLDER-"),
                        sg.FolderBrowse(button_text="Add Folder ..."),
                    ],
                    [sg.Button("Remove Selected", key="-REMOVE-SELECTED-")],
                ]
            ),
        ],
        [
            sg.Multiline(
                "",
                size=(110, 10),
                disabled=True,
                background_color="grey",
                key="-OUTPUT-",
            )
        ],
    ]

    window = sg.Window("Blue Monitor version: 1.0.0", layout)

    while True:
        event, values = window.read()
        print(event, values)
        if event == sg.WIN_CLOSED:
            break
        if event == "-ADDFOLDER-":
            folders.append(values["-ADDFOLDER-"])
            window["-FOLDER LIST-"].update(folders)
            observer = watchdog.observers.Observer()
            observer.schedule(event_handler, path=values["-ADDFOLDER-"], recursive=True)
            observer.start()
            observers[values["-ADDFOLDER-"]] = observer

    window.close()
    for observer in observers.values():
        observer.stop()
        observer.join()


if __name__ == "__main__":
    main()
    # # Process arguments
