import subprocess
from pathlib import Path
import os
from platform import system

import hashlib

from typing import TypeAlias, Dict

FileName: TypeAlias = str | bytes | os.PathLike


def find_blue() -> FileName:
    blue = "blue.vi"
    # Speed up search by looking in common dirs first
    if system() == "Windows":
        for search_path in [
            "C:\\Program Files\\National Instruments",
            "C:\\Program Files (x86)\\National Instruments\\",
            "C:\\Users\\",
        ]:
            file = find_file(blue, search_path)
            if file:
                return file
    return find_file(blue, os.path.abspath(os.sep))


def get_file_hash(path: FileName) -> str:
    with open(path, "rb") as f:
        digest = hashlib.file_digest(f, "sha256")
    return digest.hexdigest()


hashtable: Dict[FileName, str] = {}

FORMAT_VI_LOCATION = Path("C:\\Users\\SAS\\Desktop\\lv-format-on-save\\Source\\blue.vi")


def find_file(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)


def format_file(file_to_format: FileName) -> None:
    # the global hashtable is used to remember the hashes of files between calls.
    # this prevents looping behavior
    global hashtable
    try:
        last_hash = hashtable[file_to_format]
    except KeyError:
        last_hash = ""
    if get_file_hash(file_to_format) != last_hash:
        # Print that event was received and launch G-CLI VI
        print(f"Watchdog received event - {file_to_format!r}")
        # Assume Format VI is in G-CLI plugins folder
        # Above comment is no longer true. Use relative path for now. Allows development on the fly.
        subprocess.call(["g-cli.exe", FORMAT_VI_LOCATION, "--", "-d", file_to_format])
        # update last touched time
        hashtable[file_to_format] = get_file_hash(file_to_format)
