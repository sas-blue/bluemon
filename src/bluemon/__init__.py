"""Top-level package for bluemon."""

__version__ = "1.0.0"

from .gui import main  # noqa
from .gcli import *
